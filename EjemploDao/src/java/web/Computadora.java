package web;

import persistencia.Datos;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import persistencia.DAO;


@WebServlet(name="Computadora", urlPatterns={"/Computadora"})
public class Computadora extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("llegaste al doGet");                
        //ArrayList respuesta = Datos.obtener();
        ArrayList respuesta = DAO.obtener();
        //resp.setContentType("text/html;charset=UTF-8");
        resp.getWriter().println(respuesta);
        
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Estas en el POST del server porque aqui estamos en JAVA!");
    }
    
    
   
 

}
